package diff.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Base64;
import java.util.TreeMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/** A class that handles parsing Gerrit patches. */
class GerritPatchUtils {

  /**
   * Populates the content and patch information for a file.
   *
   * @param contentArray the content array from the diff
   * @param patch        the JSON array to populate with patch information
   * @param padding      the number of context lines to include around changes
   */
  public static void populateContentAndPatches(JSONObject fileResponse, JSONArray contentArray, int padding) {
    JSONArray patch = new JSONArray();

    List<Line> lines = extractLines(contentArray);
    String content = getContent(lines);
    Map<String, Integer> currentLine = initializeCurrentLineMap();

    int i = 1;
    while (i < lines.size()) {
      if (isChangeLine(lines.get(i))) {
        i = processPatch(lines, patch, currentLine, padding, i);
      } else {
        currentLine.put("-", currentLine.get("-") + 1);
        currentLine.put("+", currentLine.get("+") + 1);
        i++;
      }
    }

    fileResponse.put("patch", patch);

    Base64.Encoder encoder = Base64.getEncoder();
    fileResponse.put("content", encoder.encodeToString(content.getBytes()));
  }

  /**
   * Extracts lines from the content array.
   *
   * @param contentArray the content array from the diff
   * @return a list of lines
   */
  private static List<Line> extractLines(JSONArray contentArray) {
    List<Line> lines = new ArrayList<>();
    lines.add(null);
    for (Object obj : contentArray) {
      addLine(lines, (JSONObject) obj);
    }
    return lines;
  }

  /**
   * Concatenates added and non-modified lines
   *
   * @param lines the lines to add
   * @return the content string
   */
  private static String getContent(List<Line> lines) {
    StringBuilder concatenatedString = new StringBuilder();

    for (Line line : lines) {
      if (line != null && !line.type.equals("-")) {
        concatenatedString.append(line.content);
        concatenatedString.append("\n");
      }
    }

    return concatenatedString.toString();
  }

  /**
   * Initializes the current line map.
   */
  private static Map<String, Integer> initializeCurrentLineMap() {
    Map<String, Integer> currentLine = new TreeMap<>();
    currentLine.put("-", 1);
    currentLine.put("+", 1);
    return currentLine;
  }

  /**
   * Checks if the given line is a change line.
   */
  private static boolean isChangeLine(Line line) {
    return line.type.equals("+") || line.type.equals("-");
  }

  /**
   * Processes a patch and adds it to the patch array. The diff format recieved
   * from Gerrit
   * differs quite a bit from the desired diff format in the client. This
   * algorithm identifies
   * patches and connects patches within twice the amount of context lines. The
   * end result
   * resembles the diff view in the Gerrit web interface.
   *
   * @param lines       the list of lines
   * @param patch       the JSON array to populate with patch information
   * @param currentLine the map of current line numbers
   * @param padding     the number of context lines to include around changes
   * @param i           the current index in the lines list
   * @return the updated index in the lines list
   */
  private static int processPatch(List<Line> lines, JSONArray patch,
      Map<String, Integer> currentLine, int padding,
      int i) {
    JSONObject patchObject = new JSONObject();
    JSONArray codeParts = new JSONArray();

    int start = Math.max(i - padding, 1);
    if (start != i) {
      addCodePart(start, i, " ", lines, codeParts);
    }

    int startSrcLine = currentLine.get("-") - (i - start);
    int startTargetLine = currentLine.get("+") - (i - start);

    Boolean patchEnd = false;
    while (!patchEnd && i < lines.size()) {
      String type = lines.get(i).type;
      int from = i;
      while (i < lines.size() && lines.get(i).type.equals(type)) {
        i++;
      }

      addCodePart(from, i, type, lines, codeParts);
      currentLine.put(type, currentLine.get(type) + i - from);

      int last = i;
      while (i < lines.size() && lines.get(i).type.equals(" ")
          && i < last + (padding * 2)) {
        i++;
      }

      if (i == lines.size() - 1 || i == last + (padding * 2)) {
        patchEnd = true;
        i = last + padding;
      }

      if (i - last > 0) {
        addCodePart(last, i, " ", lines, codeParts);
        currentLine.put("-", currentLine.get("-") + i - last);
        currentLine.put("+", currentLine.get("+") + i - last);
      }
    }

    populatePatchObject(patchObject, startSrcLine, startTargetLine, codeParts, currentLine);
    patch.add(patchObject);

    return i;
  }

  /**
   * Populates a patch object with the given information.
   *
   * @param patchObject     the patch object to populate
   * @param startSrcLine    the starting source line number
   * @param startTargetLine the starting target line number
   * @param codeParts       the code parts array
   * @param currentLine     the map of current line numbers
   */
  private static void populatePatchObject(JSONObject patchObject, int startSrcLine,
      int startTargetLine, JSONArray codeParts, Map<String, Integer> currentLine) {
    patchObject.put("startSrcLine", startSrcLine);
    patchObject.put("startTargetLine", startTargetLine);
    patchObject.put("codeParts", codeParts);
    patchObject.put("srcLinesLength", currentLine.get("-") - startSrcLine);
    patchObject.put("targetLinesLength", currentLine.get("+") - startTargetLine);
  }

  /**
   * Adds a code part to the codeParts array.
   *
   * @param from      the starting line number
   * @param until     the ending line number
   * @param type      the type of lines (added, removed, or context)
   * @param lines     the list of lines
   * @param codeParts the JSON array to populate with code part information
   */
  private static void addCodePart(int from, int until, String type, List<Line> lines,
      JSONArray codeParts) {
    JSONObject codePart = new JSONObject();
    codePart.put("nbrOfLines", until - from);
    codePart.put("content", getContentAndFormat(from, until, lines));
    codePart.put("startsWith", type);
    codeParts.add(codePart);
  }

  /**
   * Gets the content of the lines from the specified range in the desired format.
   *
   * @param from  the starting line number
   * @param until the ending line number
   * @param lines the list of lines
   * @return the concatenated content of the lines
   */
  private static String getContentAndFormat(int from, int until, List<Line> lines) {
    StringBuilder concatenatedString = new StringBuilder();

    for (int i = from; i < until; i++) {
      String line = lines.get(i).content;
      concatenatedString.append(line);
      concatenatedString.append("\n");
    }

    return concatenatedString.toString();
  }

  /**
   * Adds a line to the list of lines based on the content object.
   *
   * @param lines      the list of lines
   * @param contentObj the content object containing the line information
   */
  private static void addLine(List<Line> lines, JSONObject contentObj) {
    if (contentObj.containsKey("ab")) {
      for (Object content : (JSONArray) contentObj.get("ab")) {
        lines.add(new Line((String) content, " "));
      }
    } 
    
    if (contentObj.containsKey("a")) {
      for (Object content : (JSONArray) contentObj.get("a")) {
        lines.add(new Line((String) content, "-"));
      }
    } 

    if (contentObj.containsKey("b")) {
      for (Object content : (JSONArray) contentObj.get("b")) {
        lines.add(new Line((String) content, "+"));
      }
    }
  }

  /**
   * Represents a line of code with its type (added, removed, or context) and
   * content.
   */
  private static class Line {
    String type;
    String content;

    Line(String content, String type) {
      this.content = content;
      this.type = type;
    }
  }
}