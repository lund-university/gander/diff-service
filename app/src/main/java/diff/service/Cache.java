package diff.service;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Utility class that provides in-memory caching of JSON data, with
 * persistence to a file.
 */
public class Cache {
  private Logger logger;
  private JSONObject cache;
  private String cacheFilePath;

  private FileReader fileReader;
  private ExecutorService executorService;

  public Cache(Logger logger, String cacheFilePath) {
    this.logger = logger;
    this.cacheFilePath = cacheFilePath;
    this.executorService = Executors.newSingleThreadExecutor();

    try {
      fileReader = new FileReader(cacheFilePath);

      JSONParser jsonParser = new JSONParser();
      cache = (JSONObject) jsonParser.parse(fileReader);

      logger.info("Cache re-created from file!");

    } catch (Exception e) {
      cache = new JSONObject();

      logger.info("Empty cache created!");
    }
  }

  /**
   * Checks if a specific key exists in the cache.
   *
   * @param key The key to check in the cache.
   * @return True if the key exists, false otherwise.
   */
  public synchronized boolean containsKey(String key) {
    return cache.containsKey(key);
  }

  /**
   * Retrieves a cached value as a string based on the provided key.
   *
   * @param key The key for the cached data.
   * @return The cached value as a string.
   */
  public synchronized String getCachedValueAsString(String key) {
    return cache.get(key).toString();
  }

  /**
   * Retrieves a cached value as an Object based on the provided key.
   * 
   * @param key The key for the cached data.
   * @return The cached value as an Object, or null if the key does not exist.
   */
  public synchronized Object getCachedValue(String key) {
    return cache.get(key);
  }

  /**
   * @return The keyset for the cache
   */
  public synchronized Set getCacheKeySet() {
    return cache.keySet();
  }

  /**
   * Removes a cached value
   * 
   * @param key The key for the cached data
   */
  public synchronized void removeCachedValue(String key) {
    cache.remove(key);
    asyncWriteToFile();
  }

  /**
   * Caches a JSONObject under the specified key and writes it to "cache.json".
   *
   * @param key        The key under which the JSON object is to be cached.
   * @param jsonObject The JSONObject to be cached.
   */
  public synchronized void cacheJsonObject(String key, JSONObject jsonObject) {
    cache.put(key, jsonObject);
    asyncWriteToFile();
  }

  /**
   * Caches a JSONArray under the specified key and writes it to "cache.json".
   *
   * @param key       The key under which the JSON array is to be cached.
   * @param jsonArray The JSONArray to be cached.
   */
  public synchronized void cacheJsonArray(String key, JSONArray jsonArray) {
    cache.put(key, jsonArray);
    asyncWriteToFile();
  }

  private void asyncWriteToFile() {
    executorService.submit(this::writeToFile);
  }

  private void writeToFile() {
    try (FileWriter fileWriter = new FileWriter(cacheFilePath)) {
      fileWriter.write(cache.toJSONString());
      fileWriter.flush();
      logger.info("Successfully wrote cache to file!");
    } catch (IOException e) {
        logger.severe("Unable to write cache to file: " + e.getMessage());
    }
  }
}

