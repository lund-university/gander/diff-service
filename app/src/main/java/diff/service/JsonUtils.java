package diff.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** A utils class for handling JSON Objects */
class JsonUtils {

  /**
   * Returns a JSONArray parsed from the the input String body.
   *
   * @param body the input that should be parsed to JSON.
   * @return JSONArray.
   */
  public static JSONArray getJsonArray(String body) {

    JSONParser parser = new JSONParser();

    try {
      return (JSONArray) parser.parse(body);

    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Returns a JSONObject of the String input body.
   *
   * @param body the input that should be parsed to JSON.
   * @return JSONObject.
   */
  public static JSONObject getJson(String body) {

    JSONParser parser = new JSONParser();

    try {
      return (JSONObject) parser.parse(body);

    } catch (Exception e) {
      return null;
    }
  }

   /**
   * Removes the Gerrit-specific prefix from the input JSON string if present.
   *
   * @param jsonString the JSON string that may contain the Gerrit prefix.
   * @return the JSON string without the Gerrit prefix.
   */
  public static String removeGerritPrefix(String jsonString) {
    if (jsonString.startsWith(")]}'")) {
      return jsonString.substring(4);
    } 
      
    return jsonString;
  }  
}
