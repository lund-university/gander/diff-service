package diff.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import spark.Request;
import spark.Response;

/** Class for handling the changes received from Gerrit */
public class GerritChangeService {
  private static final String GERRIT_ADDRESS = System.getenv("GERRIT_ADDRESS") != null
      ? System.getenv("GERRIT_ADDRESS")
      : "https://chromium-review.googlesource.com";
  private static final String STORED_CHANGES = System.getenv("STORED_CHANGES") != null
      ? System.getenv("STORED_CHANGES")
      : "";
  private static final String CLASS_NAME = GerritChangeService.class.getName();

  private Logger logger;
  private Cache cache;

  GerritChangeService(Logger log) {
    this.logger = log;
    cache = new Cache(logger, "/app/cache/diff-service-cache.json");
    logger.info("GerritChangeService started");

    setupStoredDiffs();
  }

   /**
   * Makes sure we have the stored diffs in cache and nothing else
   */
  private void setupStoredDiffs() {
    String[] storedList = STORED_CHANGES.split(",");
    Set<String> storedSet = new HashSet<>(Arrays.asList(storedList));

    List<String> keysToRemove = new ArrayList<>();
    Iterator<Object> cachedIds = cache.getCacheKeySet().iterator();
    while (cachedIds.hasNext()) {
      String key = cachedIds.next().toString();
      if (!storedSet.contains(key)) {
        keysToRemove.add(key);
      }
    }
    keysToRemove.forEach(key -> cache.removeCachedValue(key));

    for (String changeId : storedList) {
      if (!changeId.trim().isEmpty() && !cache.containsKey(changeId)) {
        try {
          getChangeFilesForCache(changeId.trim());
        } catch (Exception e) {
          logger.warning("Error caching change ID " + changeId + ": " + e.getMessage());
        }
      }
    }

    logger.info("Preparation of changes complete.");
  }

  /**
   * Helper method to call getChangeFiles and cache the data without
   * requiring an HTTP request.
   * 
   * @param changeID The change ID to fetch and cache.
   */
  private void getChangeFilesForCache(String changeID) {
    Request dummyRequest = new DummyRequest(changeID);
    Response dummyResponse = new DummyResponse();

    getChangeFiles(dummyRequest, dummyResponse, false);
  }

  /**
   * Fetches all the files in the change and generates patches
   * 
   * @param req the HTTP request object
   * @param res the HTTP response object
   * @param bypassCache When true, bypasses the cache check and forces fresh data to be fetched and cached
   * @return a JSON String containing all the files in the change and their patch
   *         information
   */
  public String getChangeFiles(Request req, Response res, boolean bypassCache) {
    String changeID = req.params(":change_id");
    logger.entering(CLASS_NAME, "getChangeFiles");

    if (!bypassCache && cache.containsKey(changeID)) {
      res.status(HttpURLConnection.HTTP_OK);
      String cachedString = cache.getCachedValueAsString(changeID);
      logger.exiting(CLASS_NAME, "getChangeFiles", cachedString);
      return cachedString;
    }

    String url = String.format(
        "%s/changes/%s/revisions/current/files",
        GERRIT_ADDRESS,
        changeID);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGerritConnection(url, "GET", null);

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);

        String returnString = processFileResponse(jsonString, changeID);

        logger.exiting(CLASS_NAME, "getChangeFiles", returnString);
        return returnString;
      } else {
        res.status(c.getResponseCode());
        String errGit = String.format(
            "%d: Error while fetching change files %s",
            c.getResponseCode(),
            c.getResponseMessage());
        logger.severe(errGit);
        logger.exiting(CLASS_NAME, "getChangeFiles", errGit);
        return errGit;
      }
    } catch (Exception e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to Gerrit project: %d", e.getMessage());
      logger.severe(errPath);
      logger.exiting(CLASS_NAME, "getChangeFiles", errPath);
      return errPath;
    }
  }

  /**
   * Iterates the files in the current change and fetches content and patch
   * information.
   * Fetching is done in parallel to reduce total time spent waiting for
   * responses.
   *
   * @param jsonString a JSON String containing the list of files
   * @param changeID   the ID of the change
   * @return a JSON String containing all the files in the change and their patch
   *         information
   */
  private String processFileResponse(String jsonString, String changeID) {
    logger.entering(CLASS_NAME, "processFileResponse");

    jsonString = JsonUtils.removeGerritPrefix(jsonString);

    JSONArray response = new JSONArray();
    JSONObject files = JsonUtils.getJson(jsonString);
    List<CompletableFuture<Void>> futures = new ArrayList<>();

    for (Iterator<?> iterator = files.keySet().iterator(); iterator.hasNext();) {
      String filename = (String) iterator.next();
      JSONObject file = (JSONObject) files.get(filename);
      JSONObject fileResponse = new JSONObject();
      fileResponse.put("filename", filename);

      Object insertedLines = file.get("lines_inserted") != null ? file.get("lines_inserted") : 0;
      Object deletedLines = file.get("lines_deleted") != null ? file.get("lines_deleted") : 0;

      fileResponse.put("additions", insertedLines);
      fileResponse.put("deletions", deletedLines);

      CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
        fetchContentAndPatches(fileResponse, changeID, filename);
      });

      futures.add(future);

      future.thenRun(() -> {
        fileResponse.put("path", filename);
        fileResponse.put("filename", filename);

        synchronized (response) {
          response.add(fileResponse);
        }
      });
    }

    try {
      CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }

    cache.cacheJsonArray(changeID, response);

    String result = response.toString();
    logger.exiting(CLASS_NAME, "processFileResponse", result);
    return result;
  }

  /**
   * Fetches the content of a file in a change.
   *
   * @param changeID the ID of the change
   * @param filename the name of the file
   * @return the content of the file as a base64 encoded string
   */
  private String fetchFileContent(String changeID, String filename) {
    logger.entering(CLASS_NAME, "fetchFileContent", new Object[] { changeID, filename });
    String content = "";
    try {
      String encodedFilename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
      String url = String.format(
          "%s/changes/%s/revisions/current/files/%s/content",
          GERRIT_ADDRESS,
          changeID,
          encodedFilename);

      Map<String, String> additionalHeaders = new HashMap<>();
      additionalHeaders.put("Accept", "text/plain");
      additionalHeaders.put("X-FYI-Content-Encoding", "base64");

      HttpURLConnection connection = HttpConnectionUtils.setGerritConnection(
          url,
          "GET",
          additionalHeaders);

      if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(connection.getInputStream());
        jsonString = JsonUtils.removeGerritPrefix(jsonString);

        content = jsonString;
      } else {
        logger.warning(
            "Failed to fetch file content: " +
                connection.getResponseCode() +
                " " +
                connection.getResponseMessage());
      }
    } catch (IOException e) {
      logger.severe("Error fetching file content: " + e.getMessage());
    }

    logger.exiting(CLASS_NAME, "fetchFileContent", content);
    return content;
  }

  /**
   * Fetches content and patch information for a file in a change
   *
   * @param changeID the ID of the change
   * @param filename the name of the file
   * @return a JSON Array containing the patch information
   */
  private void fetchContentAndPatches(JSONObject fileResponse, String changeID, String filename) {
    logger.entering(CLASS_NAME, "fetchContentAndPatches", new Object[] { changeID, filename });
    try {
      String encodedFilename = URLEncoder.encode(filename, StandardCharsets.UTF_8);
      String url = String.format(
          "%s/changes/%s/revisions/current/files/%s/diff",
          GERRIT_ADDRESS,
          changeID,
          encodedFilename);
      HttpURLConnection connection = HttpConnectionUtils.setGerritConnection(
          url,
          "GET",
          null);

      if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(connection.getInputStream());
        jsonString = JsonUtils.removeGerritPrefix(jsonString);

        JSONObject diff = JsonUtils.getJson(jsonString);

        if (diff != null && diff.containsKey("content")) {
          JSONArray contentArray = (JSONArray) diff.get("content");
          String change_type = (String) diff.get("change_type");

          fileResponse.put("status", change_type.toLowerCase());
          GerritPatchUtils.populateContentAndPatches(fileResponse, contentArray, 10);
        } else {
          logger.warning("Content key not found in diff or diff is null.");
        }
      } else {
        logger.warning(
            "Failed to fetch file patch: " +
                connection.getResponseCode() +
                " " +
                connection.getResponseMessage());
      }
    } catch (IOException e) {
      logger.severe("Error fetching file content: " + e.getMessage());
    } catch (Exception e) {
      String errorMessage = "Unexpected error processing patches: " + e.getMessage();
      logger.severe(errorMessage);
    }
    logger.exiting(CLASS_NAME, "fetchContentAndPatches");
  }

  /**
   * DummyResponse is a mock implementation of Spark's Response class.
   */
  private class DummyResponse extends Response {
    @Override
    public void status(int statusCode) {
        // Do nothing
    }
  };
}