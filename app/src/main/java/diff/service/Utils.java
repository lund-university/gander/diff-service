package diff.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {

  /**
   * Remove all tabs from <code>content</code> and insert space instead.
   *
   * @param content the String that will be cleaned from tabs.
   * @return the input String without tabs
   */
  static String removeTabs(String content) {
    return content.replaceAll("\\t", "    ");
  }

  /**
   * Reads and parses the response from an HTTP request as a String.
   *
   * @param res InputStream from the HTTP response.
   * @return String containing the response.
   * @throws IOException exception thrown if an I/O error occurs during reading.
   */
  public static String readResponseStream(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    in.close();
    return resp.toString();
  }
}
