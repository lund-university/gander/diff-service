package diff.service;

import spark.Request;

/**
 * DummyRequest is a mock implementation of Spark's Request class.
 * It is used to simulate an HTTP request for internal calls to 
 * methods like getChangeFiles without requiring a real HTTP request.
 */
public class DummyRequest extends Request {
    private final String changeId;

    public DummyRequest(String changeId) {
        this.changeId = changeId;
    }

    /**
     * Overrides the params method to return the change ID.
     * This method provides the required change_id path parameter for
     * the getChangeFiles method in GerritChangeService.
     *
     * @param paramName The name of the parameter to retrieve.
     * @return The change ID if the parameter name is ":change_id"; null otherwise.
     */
    @Override
    public String params(String paramName) {
        if (paramName.equals(":change_id")) {
            return changeId;
        }
        return null;
    }
}
