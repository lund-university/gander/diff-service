package diff.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Map;

/**
 * A utils class for setting up connections and authentications to GitHub and
 * Gerrit. For example:
 *
 * <p>
 * A HTTPURLConnection without headers.
 *
 * <p>
 * A HTTPURLConnection with "application/json" as content-type.
 *
 * <p>
 * A Connection with the GitHub Authentication token.
 */
class HttpConnectionUtils {

  /**
   * Used for establishing connection with content-type header set to
   * application/json.
   *
   * @param urlString the URL String
   * @param method    the HTTP request type, for example "GET".
   * @return HTTPURLConnection object
   * @throws IOException exception thrown by this method.
   */
  public static HttpURLConnection setConnection(String urlString, String method)
      throws IOException {
    HttpURLConnection c = HttpConnectionUtils.setConnectionNoHeaders(urlString, method);
    c.setRequestProperty("Content-Type", "application/json");
    c.setRequestProperty("Accept", "application/json");
    return c;
  }

  /**
   * Used for establishing connection with no set headers.
   *
   * @param urlString the URL String.
   * @param method    the HTTP request type, for example "GET".
   * @return HttpURLConnection object.
   * @throws IOException exception thrown.
   * 
   */
  public static HttpURLConnection setConnectionNoHeaders(String urlString, String method)
      throws IOException {
    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    return c;
  }

  /**
   * Used for establishing connection with content-type header set to
   * application/json and GitHub
   * authentication token provided. Used when communicating with GitHub.
   *
   * @param urlString the URL String.
   * @param method    the HTTP request type, for example "GET".
   * @return HTTPURLConnection object.
   * @throws IOException exception thrown
   * 
   */
  public static HttpURLConnection setGithubConnection(String urlString, String method)
      throws IOException {
    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    c.setRequestProperty("Accept", "application/vnd.github.v3+json");
    c.setRequestProperty("Authorization", getBasicAuth());
    return c;
  }

  /**
   * Establishes a connection to a Gerrit server, using the appropriate
   * authentication method if provided.
   * 
   * If the OAuth token is available, it uses Bearer token authentication. If the
   * OAuth token is not available,
   * it checks for a username and password in the environment variables and uses
   * Basic authentication.
   *
   * @param urlString         the URL String.
   * @param method            the HTTP request type, for example "GET".
   * @param additionalHeaders a map of additional headers to be set on the
   *                          connection.
   * @return HttpURLConnection object.
   * @throws IOException exception thrown if an I/O error occurs.
   */
  public static HttpURLConnection setGerritConnection(String urlString, String method,
      Map<String, String> additionalHeaders)
      throws IOException {

    if (isOAuthPresent() || isUsernamePasswordPresent()) {
      urlString = urlString.replace("/changes", "/a/changes");
    }

    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    c.setRequestProperty("Accept", "application/json");

    if (additionalHeaders != null) {
      for (Map.Entry<String, String> entry : additionalHeaders.entrySet()) {
        c.setRequestProperty(entry.getKey(), entry.getValue());
      }
    }

    if (isOAuthPresent()) {
      String oauthToken = System.getenv("OAUTH_TOKEN");

      c.setRequestProperty("Authorization", "Bearer " + oauthToken);
    } else if (isUsernamePasswordPresent()) {
      String username = System.getenv("GERRIT_USERNAME");
      String password = System.getenv("GERRIT_PASSWORD");

      String encodedCredentials = Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
      c.setRequestProperty("Authorization", "Basic " + encodedCredentials);
    }

    return c;
  }

  /**
   * Checks whether an OAuth token is set as an environment variable.
   */
  private static Boolean isOAuthPresent() {
    String oauthToken = System.getenv("OAUTH_TOKEN");
    return oauthToken != null && !oauthToken.isEmpty();
  }

  /**
   * Checks whether an username and password is set as environment variables.
   */
  private static Boolean isUsernamePasswordPresent() {
    String username = System.getenv("GERRIT_USERNAME");
    String password = System.getenv("GERRIT_PASSWORD");
    return username != null && !username.isEmpty() && password != null && !password.isEmpty();
  }

  /**
   * Reads the credentials.txt file and retrieves the Auth token. (See more
   * information about the
   * credentials.txt file in README.md).
   *
   * @return the Authentication token as requested by GitHub.
   */
  private static String getBasicAuth() {
    try {
      Path credentialsPath = FileSystems.getDefault().getPath("credentials.txt").toAbsolutePath();
      String userCredentials = new String(Files.readAllBytes(credentialsPath));
      String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
      return basicAuth;

    } catch (Exception e) {
      return null;
    }
  }
}
