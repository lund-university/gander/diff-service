package diff.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class for handling the pull requests received from GitHub */
class PullRequestService {

  private static final String GIT_OWNER = System.getenv("GIT_OWNER") != null ? System.getenv("GIT_OWNER") : "granttitus";
  private static final String GIT_REPO = System.getenv("GIT_REPO") != null ? System.getenv("GIT_REPO") : "FlappyBird";
  private static final String GIT_ADDRESS = System.getenv("GIT_ADDRESS") != null ? System.getenv("GIT_ADDRESS") : "https://api.github.com";
  private static final String GIT_REPO_PATH = "repos";
  private static final String GIT_PULLS = "pulls";
  private static final String GIT_COMPARE = "compare";
  private static final String FILE_PATH = "files";
  private HashMap<String, String> pullRequestFilesCache = new HashMap<String, String>();

  private Logger logger;

  PullRequestService(Logger log) {
    this.logger = log;
    logger.info("PullRequestService started");
  }

  /**
   * Generates patches for all the files in the pull request
   *
   * @param req the HTTP request object
   * @param res the HTTP response object
   * @return a JSON String containing all the files in the pull request and their patch information
   */
  public String getPullRequestFiles(Request req, Response res) {
    String owner = req.params(":owner") != null ? req.params(":owner") : GIT_OWNER;
    String repo = req.params(":repo") != null ? req.params(":repo") : GIT_REPO;
    String pullNumber = req.params(":pull_number");

    // Check if we already have this response in the cache
    if (repo.equals(GIT_REPO) && pullRequestFilesCache.containsKey(pullNumber)) {
      res.status(HttpURLConnection.HTTP_OK);
      return pullRequestFilesCache.get(pullNumber);
    }

    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s/%s?diff=split",
            GIT_ADDRESS, GIT_REPO_PATH, owner, repo, GIT_PULLS, pullNumber, FILE_PATH);

    System.out.println(url);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        String returnString = processFileResponseAndAddPatches(jsonString);
        if (repo.equals(GIT_REPO)) {
          pullRequestFilesCache.put(pullNumber, returnString);
          String loggedPullRequestFile = String.format("Pull request #%s is cached", pullNumber);
          logger.severe(loggedPullRequestFile);
        }
        return returnString;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while fetching pull request files %s",
                c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (Exception e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Iterates the files in the current pull request and adds diff patch information for each file
   *
   * @param jsonString a JSON String containing the list of files
   * @return a JSON String containing all the files in the pull request and their patch information
   */
  private String processFileResponseAndAddPatches(String jsonString) {
    JSONArray response = new JSONArray();
    JSONArray arrayToParse = JsonUtils.getJsonArray(jsonString);
    Iterator<JSONObject> iterator = arrayToParse.iterator();
    while (iterator.hasNext()) {

      JSONObject file = iterator.next();
      JSONObject newObj = new JSONObject();
      newObj.put("filename", file.get("filename"));
      newObj.put("status", file.get("status"));
      newObj.put("additions", file.get("additions"));
      newObj.put("deletions", file.get("deletions"));
      String contentUrl = (String) file.get("contents_url");
      addFileContent(contentUrl, newObj);

      if (file.get("patch") != null) {
        JSONArray patch = GitHubPatchUtils.getPatchArray(file.get("patch").toString());
        newObj.put("patch", patch);
      }

      response.add(newObj);
    }
    return response.toString();
  }

  /**
   * Fetches the content of a file from <code>url</code> and adds it to <code>obj</code>
   *
   * @param url the url to the file content
   * @param obj the object to add the content to
   */
  private void addFileContent(String url, JSONObject obj) {
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        JSONObject contentObj = JsonUtils.getJson(jsonString);
        String content = (String) contentObj.get("content");
        content = Utils.removeTabs(content);
        String path = (String) contentObj.get("path");
        obj.put("content", content);
        obj.put("path", path);

      } else {

        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
      }
    } catch (IOException e) {

      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
    }
  }

  /**
   * Fetches a pull request from the GitHub API
   *
   * @param req the HTTP request object
   * @param res the HTTP response object
   * @return A JSON String with the pull request information
   */
  public String getPullRequest(Request req, Response res) {
    String pullNumber = req.params(":pull_number");
    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s",
            GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, GIT_PULLS, pullNumber);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        return processResponse(jsonString);
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Process the response from the GitHub API when fetching for a pull request
   *
   * @param jsonString the response form the GitHub API
   * @return A JSON String containing the content of the response
   */
  private String processResponse(String jsonString) {
    try {
      JSONObject response = new JSONObject();

      JSONObject obj = JsonUtils.getJson(jsonString);
      JSONObject head = (JSONObject) obj.get("head");
      String headSha = (String) head.get("sha");
      String headRef = (String) head.get("ref");
      response.put("head_sha", headSha);
      response.put("head_ref", headRef);

      JSONObject base = (JSONObject) obj.get("base");
      String baseSha = (String) base.get("sha");
      String baseRef = (String) base.get("ref");
      response.put("base_sha", baseSha);
      response.put("base_ref", baseRef);
      JSONArray patchArray = compareHeadAndBase(headSha, baseSha);
      response.put("patch_list", patchArray);

      return response.toString();
    } catch (Exception e) {

      return "Problem with processing response";
    }
  }

  /**
   * Compares two branches and returns the differences beetween them
   *
   * @param head the head branch
   * @param base the base branch
   * @return a JSONArray containing the diff patch for each file that is changed in at least one of
   *     the branches
   */
  private JSONArray compareHeadAndBase(String head, String base) {
    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s...%s",
            GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, GIT_COMPARE, base, head);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        return GitHubPatchUtils.processCompareCommits(jsonString);
      } else {

        String errGit =
            String.format(
                "%d: Error while geting from git: %s", c.getResponseCode(), c.getResponseMessage());
        return null;
      }

    } catch (Exception e) {
      String errExc = String.format("Unable to compare commits: %s", e.getMessage());
      return null;
    }
  }
}
