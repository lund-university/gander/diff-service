package diff.service;

import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/** A class that handles parsing GitHub patches. */
class GitHubPatchUtils {

  /**
   * Handles the response from the GitHub API from comparing different branches.
   *
   * @param s the API response that is being processed. The response contains a list of files that
   *     were included in a change, e.g. a pull request.
   * @return a JSONArray containg information about the files and diff patches.
   */
  public static JSONArray processCompareCommits(String s) {

    JSONArray resultArray = new JSONArray();

    JSONObject obj = JsonUtils.getJson(s);
    JSONArray files = (JSONArray) obj.get("files");

    Iterator<JSONObject> iterator = files.iterator();

    while (iterator.hasNext()) {

      JSONObject file = iterator.next();
      JSONObject newObj = new JSONObject();
      newObj.put("filename", file.get("filename"));
      newObj.put("status", file.get("status"));
      newObj.put("additions", file.get("additions"));
      newObj.put("deletions", file.get("deletions"));

      if (file.get("patch") != null) {
        JSONArray patch = getPatchArray(file.get("patch").toString());
        newObj.put("patch", patch);
      }

      resultArray.add(newObj);
    }
    return resultArray;
  }

  /**
   * Generates and returns a JSONArray containing the start value, lenght and content for each code
   * part in the current patch. The rows of the input String is parsed to find these values. Rows
   * with the same start symbol is bundled together. Each row in the bundle contributes with +1 to
   * nbrOfLines. The content of the rows are merged together. Each section with multiple rows with
   * the same start symbol is now seen as a code part.
   *
   * @param input a String that is parsed to find the information we want to save
   * @return a JSONArray consisting of start value, lenght and content for each code part in the
   *     patch.
   */
  private static JSONArray getModifications(String input) {
    JSONArray result = new JSONArray();
    StringBuilder contentBuilder = new StringBuilder();
    String[] lines = input.split("\n");
    char lastLineStart = '?';
    int nbrOfLines = 0;
    for (int i = 1; i < lines.length; i++) {
      String currentLine = lines[i];

      if (!currentLine.startsWith(String.valueOf(lastLineStart))) {
        /*
        If the start symbol on this line differ from the start symbol 
        on the last line, add the information about the last code part 
        and start to collect information about the next one. 
        */
        if (nbrOfLines > 0) {
          JSONObject patchJson = new JSONObject();
          patchJson.put("startsWith", String.valueOf(lastLineStart));
          patchJson.put("nbrOfLines", nbrOfLines);
          String content = Utils.removeTabs(contentBuilder.toString());
          patchJson.put("content", content);
          result.add(patchJson);

          contentBuilder = new StringBuilder();
          nbrOfLines = 0;
        }
      }
      nbrOfLines++;

      contentBuilder.append(currentLine.substring(1));
      contentBuilder.append("\n");

      if (currentLine.length() > 0) {
        lastLineStart = currentLine.charAt(0);
      }
    }
    JSONObject patchJson = new JSONObject();

    patchJson.put("startsWith", String.valueOf(lastLineStart));
    patchJson.put("nbrOfLines", nbrOfLines);
    String content = Utils.removeTabs(contentBuilder.toString());
    patchJson.put("content", content);
    result.add(patchJson);

    return result;
  }

  /**
   *Iterates a array of the content and calculates the number of chars to a change (columnNbr).
   * The array contains either a changed part of the code or the content between changes. Returns a
   * JSONArray with the start columns and end columns for each change.
   *
   * @param codeLine a String[] were every other item is a change, and every other item is the
   *     content between changes.
   * @param startColumnNbr the number of chars from the start of the code part.
   * @param columnList the JSONArray that keeps track of the startColumn and endColumn.
   * @return JSONArray
   */
  private static JSONArray findAndAddChangedParts(
      String[] codeLine, int startColumnNbr, JSONArray columnList) {
    boolean isStartCoord = true;
    JSONObject coordinates = null;

    int columnNbr = startColumnNbr;

    for (int i = 0; i < codeLine.length; i++) {
      String code = codeLine[i];

      columnNbr += code.length();

      if (isStartCoord) {

        coordinates = new JSONObject();
        coordinates.put("startColumn", columnNbr);
        isStartCoord = false;
      } else {
        coordinates.put("endColumn", columnNbr);
        columnList.add(coordinates);

        isStartCoord = true;
      }
    }
    return columnList;
  }

  /**
   * Generates and returns a tag that is not a part of <code>content</code>.
   *
   * @param content the String that can not contain the tag.
   * @return a String that is not contained in <code>content</code>.
   */
  private static String createUniqueTag(String content) {
    String uniqueTag;
    do {
      uniqueTag = UUID.randomUUID().toString();
    } while (content.contains(uniqueTag));
    return uniqueTag;
  }

  /**
   * Adds information about exactly which chars have been changed in each patch. This is done by
   * adding the number of chars from the beginning of the patch until the start/end of a change.
   *
   * @param modifications the JSONArray that the coordinates are added to.
   */
  private static void addChangeCoordinates(JSONArray modifications) {
    Iterator<JSONObject> it = modifications.iterator();
    JSONObject oldObject = new JSONObject();
    oldObject.put("startsWith", "");
    while (it.hasNext()) {
      JSONObject newObject = (JSONObject) it.next();
      String newFirstSymbol = (String) newObject.get("startsWith");
      String oldFirstSymbol = (String) oldObject.get("startsWith");
      /*The column information is only added if two consecutive modifications are
      an addition and a deletion.The column information is used to determine exactly which
      part of a row have been changed, and this is only interesting when a deletion row and
      an addition row is compared.
      */
      if ((newFirstSymbol.equals("+") && oldFirstSymbol.equals("-"))
          || (newFirstSymbol.equals("-") && oldFirstSymbol.equals("+"))) {
        String oldContent = (String) oldObject.get("content");
        String[] oldArr = oldContent.split("\n");
        String newContent = (String) newObject.get("content");
        String[] newArr = newContent.split("\n");

        JSONArray oldColumnList = new JSONArray();
        JSONArray newColumnList = new JSONArray();
        addColumnInfoToArrays(oldArr, newArr, oldColumnList, newColumnList);

        if (!oldColumnList.isEmpty()) {
          oldObject.put("coordinates", oldColumnList);
        }

        if (!newColumnList.isEmpty()) {
          newObject.put("coordinates", newColumnList);
        }
      }
      oldObject = newObject;
    }
  }

  /**
   * Add column info to either the source or the target.
   *
   * @param arr the String[] to get the information from.
   * @param columnList the JSONArray to add the information to.
   */
  private static void addColumnInfoToOneSide(String[] arr, JSONArray columnList) {
    JSONObject coordinates = new JSONObject();
    coordinates.put("startColumn", 0);
    int wordLength = arr[0].length();

    coordinates.put("endColumn", wordLength);
    columnList.add(coordinates);
  }

  /**
   * Add the column information (the number of chars from the start of the patch until a change) for
   * both the source and the target version of the file.
   *
   * @param oldArr a String array containg the content of the source file.
   * @param newArr a String array containing the content of the target file.
   * @param oldColumnList the JSONArray were the column information for the source file is kept.
   * @param newColumnList the JSONArray were the column information for the target file is kept.
   */
  private static void addColumnInfoToArrays(
      String[] oldArr, String[] newArr, JSONArray oldColumnList, JSONArray newColumnList) {
    int i = 0;
    int lastOldNbr = 0;
    int lastNewNbr = 0;
    int oldLength = oldArr.length;
    int newLength = newArr.length;

    // If one of the arrays are empty, everything in the other
    // array at position zero should be added
    if (oldLength == 0 && newLength > 0) {
      addColumnInfoToOneSide(newArr, newColumnList);
      return;
    } else if (newLength == 0 && oldLength > 0) {
      addColumnInfoToOneSide(oldArr, oldColumnList);
      return;
    }

    while (i < newArr.length && i < oldArr.length) {
      String newLine = newArr[i];
      String oldLine = oldArr[i];
      String oldTag = createUniqueTag(oldLine);
      String newTag = createUniqueTag(newLine);
      i++;
      List<DiffRow> rows = generateDiff(oldLine, newLine, oldTag, newTag);

      StringBuilder oldBuilder = new StringBuilder();
      StringBuilder newBuilder = new StringBuilder();
      for (DiffRow row : rows) {
        oldBuilder.append(row.getOldLine());
        newBuilder.append(row.getNewLine());
      }
      String oldStringEscaped = oldBuilder.toString();
      String oldString = StringEscapeUtils.unescapeHtml4(oldStringEscaped);
      String newStringEscaped = newBuilder.toString();
      String newString = StringEscapeUtils.unescapeHtml4(newStringEscaped);
      double limit = 0.5; // The limit of when we stop adding column information
      /*
      If too much differ between the source and target line no column information is added.
      If the lines in the different versions are too different, we do not show exactly which 
      parts have been changed.
      */
      if (isUnderLimit(oldString, oldTag, limit) && isUnderLimit(newString, newTag, limit)) {

        findAndAddChangedParts(oldString.split(oldTag), lastOldNbr, oldColumnList);
        findAndAddChangedParts(newString.split(newTag), lastNewNbr, newColumnList);
      }
      lastOldNbr += oldLine.length();
      lastNewNbr += newLine.length();
    }
  }

  /**
   * Checks if the part of <code>content</code> that is a change is less than <code>limit</code>.
   *
   * @param content the string that contains the changes.
   * @param tag the uniques string that marks were changes begins and stops.
   * @param limit a double that decides how much change is sallowed.
   * @return true if the amount of change is under <code>limit</code>, else false.
   */
  private static boolean isUnderLimit(String content, String tag, double limit) {
    if (content.length() == 0) {
      return true;
    }

    String[] stringArray = content.split(tag);
    double stringLength = content.replace(tag, "").length();
    double nbrOfChangedChars = 0;
    for (int i = 0; i < stringArray.length; i++) {
      if (i % 2 != 0) {
        nbrOfChangedChars = nbrOfChangedChars + stringArray[i].length();
      }
    }

    double percentage = (nbrOfChangedChars / stringLength);
    if (percentage < limit) {
      return true;
    }
    return false;
  }

  /**
   * Use diff-utils to find exactly which part of each row that has been changed.
   *
   * @param oldContent the content before the change
   * @param newContent the content after the change
   * @param oldTag a String that does not occur in <code>oldContent</code>
   * @param newTag a String that does not occur in <code>newContent</code>
   * @return a List of DiffRow objects
   */
  private static List<DiffRow> generateDiff(
      String oldContent, String newContent, String oldTag, String newTag) {
    List<String> previous = Arrays.asList(oldContent.split("\n"));
    List<String> current = Arrays.asList(newContent.split("\n"));
    try {

      DiffRowGenerator generator =
          DiffRowGenerator.create()
              .showInlineDiffs(true)
              .inlineDiffByWord(true)
              .oldTag(f -> oldTag)
              .newTag(f -> newTag)
              .build();

      return generator.generateDiffRows(previous, current);
    } catch (Exception e) {

      return new ArrayList<DiffRow>();
    }
  }

  /**
   * Takes a a GitHub patch string, parses it and returns a JSONArray with the patch informaton.
   *
   * @param patchString a JSON String containing the patch information from the GitHub API.
   * @return a JSONArray with patch information about the file
   */
  public static JSONArray getPatchArray(String patchString) {

    JSONArray patchArray = new JSONArray();
    if (patchString != null) {
      String[] arrPatchString = patchString.split("@@");

      for (int k = 1; k < arrPatchString.length; k += 2) {
        JSONObject jsonPatch = new JSONObject();
        getLines(arrPatchString[k], jsonPatch);
        JSONArray modifications = getModifications(arrPatchString[k + 1]);
        addChangeCoordinates(modifications);

        jsonPatch.put("codeParts", modifications);
        patchArray.add(jsonPatch);
      }
    }
    return patchArray;
  }

  /**
   * Adds the start line and line length of each patch to <code>jsonPatch</code> for both the src
   * and target version of the file
   *
   * @param input the String that is parsed to find the line information
   * @param jsonPatch the JSONObject that the line information is added to
   */
  private static void getLines(String input, JSONObject jsonPatch) {

    String[] srcAndTargetArr = input.split(" ");
    String srcLines = srcAndTargetArr[1];
    srcLines = srcLines.substring(1); // Remove the '-' sign
    String targetLines = srcAndTargetArr[2];
    targetLines = targetLines.substring(1); // Remove the '+' sign
    String[] srcLinesArr = srcLines.split(",");
    String[] targetLinesArr = targetLines.split(",");
    int startSrcLine = Integer.valueOf(srcLinesArr[0]);

    int srcLinesLength = (srcLinesArr.length > 1) ? Integer.valueOf(srcLinesArr[1]) : 0;
    int startTargetLine = Integer.valueOf(targetLinesArr[0]);
    int targetLinesLength = (targetLinesArr.length > 1) ? Integer.valueOf(targetLinesArr[1]) : 0;
    jsonPatch.put("startSrcLine", startSrcLine);
    jsonPatch.put("srcLinesLength", srcLinesLength);
    jsonPatch.put("startTargetLine", startTargetLine);
    jsonPatch.put("targetLinesLength", targetLinesLength);
  }
}
