package diff.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

class DiffService {
  private static final String GIT_OWNER = System.getenv("GIT_OWNER") != null ? System.getenv("GIT_OWNER") : "granttitus";
  private static final String GIT_REPO = System.getenv("GIT_REPO") != null ? System.getenv("GIT_REPO") : "FlappyBird";
  private static final String GIT_ADDRESS = System.getenv("GIT_ADDRESS") != null ? System.getenv("GIT_ADDRESS") : "https://api.github.com";
  private static final String GIT_REPO_PATH = "repos";
  private static final String GIT_COMPARE = "/compare";
  private Logger logger;
  private static final String CLASS_NAME = DiffService.class.getName();

  public DiffService(Logger log) {
    this.logger = log;
    logger.info("DiffService started");
  }

  public String compareFiles(Request req, Response res) {
    logger.entering(CLASS_NAME, "compareFiles");
    JSONObject body = JsonUtils.getJson(req.body());
    String src = (String) body.get("src");
    String target = (String) body.get("target");
    String srcPath = createTmpPath(src);
    String targetPath = createTmpPath(target);

    JSONObject result = new JSONObject();

    if (srcPath == null || targetPath == null) {
      result.put("status", "ERROR");
      result.put("message", "problem with generating the patch");
      res.status(404);

      logger.exiting(CLASS_NAME, "compareFiles", result);
      return result.toString();
    }

    String patch = runDiffCommand(srcPath, targetPath);

    if (patch == null) {
      result.put("status", "ERROR");
      result.put("message", "problem with generating the patch");
      res.status(404);
      logger.severe("error: problem with generating the patch");
      return result.toString();
    }
    res.status(200);
    result.put("status", "SUCCESS");
    result.put("patch", patch);
    logger.exiting(CLASS_NAME, "compareFiles", result);
    return result.toString();
  }

  private String runDiffCommand(String srcPath, String targetPath) {
    logger.entering(CLASS_NAME, "runDiffCommand");
    String command = "git diff --no-index " + srcPath + " " + targetPath;

    try {

      Process proc = Runtime.getRuntime().exec(command);

      BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

      StringBuilder sb = new StringBuilder();
      int headerLines = 4;
      String line = "";
      int i = 0;
      while ((line = reader.readLine()) != null) {
        i++;
        if (i > headerLines) {
          sb.append(line + "\n");
        }
      }

      proc.waitFor();
      logger.exiting(CLASS_NAME, "runDiffCommand", sb.toString());
      return sb.toString();
    } catch (Exception e) {
      String err = String.format("Error with diff command: %s", e.getMessage());
      logger.severe(err);
      logger.exiting(CLASS_NAME, "runDiffCommand", null);
      return null;
    }
  }

  private String createTmpPath(String content) {
    logger.entering(CLASS_NAME, "createTmpPath", content);
    try {

      Path tmpFilePath = Files.createTempFile(null, ".txt");
      File tmpFile = tmpFilePath.toFile();
      tmpFile.deleteOnExit();
      FileWriter fileWriter = new FileWriter(tmpFile);
      fileWriter.write(content);
      fileWriter.close();
      logger.exiting(CLASS_NAME, "createTmpPath", tmpFilePath);
      return tmpFilePath.toString();

    } catch (Exception e) {
      String err = String.format("error: %s", e.getMessage());
      logger.severe(err);
      logger.exiting(CLASS_NAME, "createTmpPath", null);
      return null;
    }
  }

  public String compareCommits(Request req, Response res) {
    String owner = req.params(":owner") != null ? req.params(":owner") : GIT_OWNER;
    String repo = req.params(":repo") != null ? req.params(":repo") : GIT_REPO;
    String commits = req.params(":commits");

    String url =
        String.format("%s/%s/%s/%s/%s/%s", GIT_ADDRESS, GIT_REPO_PATH, owner, repo, GIT_COMPARE, commits);
    logger.entering(CLASS_NAME, "compareCommits", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "compareCommits", jsonString);
        return GitHubPatchUtils.processCompareCommits(jsonString).toString();
      } else {
        res.status(c.getResponseCode());

        String errGit =
            String.format(
                "%d: Error while geting from git: %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        logger.exiting(CLASS_NAME, "compareCommits", errGit);
        return errGit;
      }

    } catch (Exception e) {
      String errExc = String.format("Unable to compare commits: %d", e.getMessage());
      logger.severe(errExc);
      logger.exiting(CLASS_NAME, "compareCommits", errExc);
      return errExc;
    }
  }

  public String getRateLimit(Request req, Response res) {
    String url = "https://api.github.com/rate_limit";
    logger.entering(CLASS_NAME, "getRateLimit", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = Utils.readResponseStream(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "getRateLimit", jsonString);
        return jsonString;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting rate limit from git %s",
                c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        logger.exiting(CLASS_NAME, "getRateLimit", errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errExc);
      logger.exiting(CLASS_NAME, "getRateLimit", errExc);
      return errExc;
    }
  }
}
