package diff.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PatchTest {

  private String patchString =
      "@@ -2,17 +2,21 @@\n"
          + " \n"
          + " public class App {\n"
          + " \n"
          + "-    public static void main (String[] args) {\n"
          + "+    public static int WIDTH = 500;\n"
          + "+    public static int HEIGHT = 520;\n"
          + "+\n"
          + "+    public static void main(String[] args) {\n"
          + "         JFrame frame = new JFrame();\n"
          + "         frame.setVisible(true);\n"
          + "         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n"
          + "-        frame.setSize(500, 500);\n"
          + "         frame.setLocationRelativeTo(null);\n"
          + " \n"
          + "         Keyboard keyboard = Keyboard.getInstance();\n"
          + "         frame.addKeyListener(keyboard);\n"
          + " \n"
          + "         GamePanel panel = new GamePanel();\n"
          + "         frame.add(panel);\n"
          + "+        frame.setResizable(false);\n"
          + "+        frame.setSize(WIDTH, HEIGHT);\n"
          + "     }\n"
          + " }";

  private JSONArray patchArray;
  private JSONObject patch;
  JSONArray codeParts;

  @BeforeEach
  public void setUp() {
    System.out.println("In before");
    patchArray = GitHubPatchUtils.getPatchArray(patchString);
    patch = (JSONObject) patchArray.get(0);
    codeParts = (JSONArray) patch.get("codeParts");
  }

  @Test
  public void testPatchArrayLength() {

    int arrayLength = patchArray.size();
    int expectedLength = 1;
    assertEquals(arrayLength, expectedLength);
  }

  @Test
  public void testNbrOfLines() {
    int expectedSrcLinesLength = 17;
    int expectedStartTagretLine = 2;
    int expectedTargetLinesLength = 21;
    int expectedStartSrcLine = 2;

    assertEquals(expectedStartSrcLine, patch.get("startSrcLine"));
    assertEquals(expectedStartTagretLine, patch.get("startTagretLine"));
    assertEquals(expectedSrcLinesLength, patch.get("srcLinesLength"));
    assertEquals(expectedTargetLinesLength, patch.get("targetLinesLength"));
  }

  @Test
  public void testNbrOfCodeParts() {

    int codePartLength = codeParts.size();
    int expectedLength = 8;
    assertEquals(codePartLength, expectedLength);
  }

  @Test
  public void testCodePartsNbrOfLines() {

    JSONObject firstCodePart = (JSONObject) codeParts.get(0);
    int firstNbrOfLines = (int) firstCodePart.get("nbrOfLines");
    int expectedfirstNbrOfLines = 3;

    JSONObject secondCodePart = (JSONObject) codeParts.get(1);
    int secondNbrOfLines = (int) secondCodePart.get("nbrOfLines");
    int expectedSecondNbrOfLines = 1;

    assertEquals(firstNbrOfLines, expectedfirstNbrOfLines);
    assertEquals(secondNbrOfLines, expectedSecondNbrOfLines);
  }

  @Test
  public void testCodePartsStartsWith() {

    JSONObject secondCodePart = (JSONObject) codeParts.get(1);
    String secondFirstSymbol = (String) secondCodePart.get("startsWith");
    String expectedSecondFirstSymbol = "-";
    JSONObject thirdCodePart = (JSONObject) codeParts.get(2);
    String thirdFirstSymbol = (String) thirdCodePart.get("startsWith");
    String expectedThirdFirstSymbol = "+";

    assertEquals(secondFirstSymbol, expectedSecondFirstSymbol);
    assertEquals(thirdFirstSymbol, expectedThirdFirstSymbol);
  }

  @Test
  public void testCodePartContent() {

    JSONObject thirdCodePart = (JSONObject) codeParts.get(2);
    String content = (String) thirdCodePart.get("content");
    String expectedContent =
        "    public static int WIDTH = 500;\n"
            + "    public static int HEIGHT = 520;\n"
            + "\n"
            + "    public static void main(String[] args) {\n";
    assertEquals(content, expectedContent);
  }
}
