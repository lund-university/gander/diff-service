package diff.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PatchTestEmptyLine {

  String patchString =
      "@@ -24,7 +25,7 @@ protected void paintComponent (Graphics g) {\n"
          + "         g.setColor(Color.GREEN);\n"
          + "         g.fillRect(bird.x, bird.y, bird.width, bird.height);\n"
          + "     }\n"
          + "-\n"
          + "+    \n"
          + // Line with space
          "     public void run () {\n"
          + "         try {\n"
          + "             while (true) {";

  private JSONArray patchArray;
  private JSONObject patch;
  JSONArray codeParts;
  JSONObject lineWithSpace;

  @BeforeEach
  public void setUp() {
    System.out.println("In before");
    patchArray = GitHubPatchUtils.getPatchArray(patchString);
    patch = (JSONObject) patchArray.get(0);
    codeParts = (JSONArray) patch.get("codeParts");
    System.out.println(codeParts);
    lineWithSpace = (JSONObject) codeParts.get(2);
  }

  @Test
  public void testStartsWith() {
    String expectedStartsWith = "+";
    String startsWith = (String) lineWithSpace.get("startsWith");
    assertEquals(startsWith, expectedStartsWith);
  }

  @Test
  public void testCoordinatesLength() {
    int expectedLength = 1;
    JSONArray coordinates = ((JSONArray) lineWithSpace.get("coordinates"));
    Assertions.assertNotNull(coordinates);
    int length = coordinates.size();
    assertEquals(length, expectedLength);
  }

  @Test
  public void testColumnValues() {
    int expectedStartColumn = 0;
    int expectedEndColumn = 4;
    JSONArray coordinateList = ((JSONArray) lineWithSpace.get("coordinates"));
    JSONObject coordinates = (JSONObject) coordinateList.get(0);
    int startColumn = (int) coordinates.get("startColumn");
    int endColumn = (int) coordinates.get("endColumn");

    assertEquals(expectedStartColumn, startColumn);
    assertEquals(expectedEndColumn, endColumn);
  }
}
