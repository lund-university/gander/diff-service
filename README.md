# diff-service

The Diff Service is used to generate patch information about the files that are displayed in the client. For more information about what patches are, see further below.

## Run
To run the diff service, run:

Windows: `gradle run`

Mac: `./gradlew run`

To build a docker image, run:

Windows: `gradle docker`

Mac: `./gradlew docker`

## GitHub Authentication
Diff Service needs a file called `credentials.txt` to be able to fetch comments and work properly with GitHub. If you don't have a personal access token in the header of your HTTP-request, after approx. 50 requests you will get an error code 403: Forbidden. 

This require you to:
* Have a GitHub account
* [Follow this tutorial for creating a GitHub personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token). I recommend having a separate one for working with Gander.  
* Save the information in a file called `credentials.txt`. It should be located inside the `/app` folder. 
* The file should contain `username:AuthToken`, where username is your username on GitHub and AuthToken the token you just created. The username and the AuthToken should be separated by a `:`.
* This token is used in more services, so copy the file into them as well. Read the separate `README.md` files to see if the services need the authentication token. It is okay to use the same access token in multiple service. username:AuthToken

## API Endpoints:

When the service is running, it can handle the following API calls:

To get test the ping-pong service: `curl -X GET localhost:8004/ping`

To compare two files: `curl -d '{"src":"content of src", "target":"content of target"}' -X POST localhost:8004/compareFiles`

To compare two commits: `curl -X GET localhost:8004/diff/compare/commit1...commit2`
Or with an example for the github repo used: `curl -X GET localhost:8004/diff/compare/9bed4acacdd980541b181ebdc099aff89bd6f16e...6ca4588b19a236796afef307d0b10ec1fceb37d0`

To get files and patch from a pull request: `curl -X GET localhost:8004//diff/pulls/{pull_number}/files`

## Patches
 A patch is an instruction on which changes need to be done to turn one file into another. 
 The patches are therefore used to find the differences between different files, e.g. the version of a file before and after a pull request.

Patches are fetched from GitHub and have this format:
``` 
@@ -2,5 +2,6 @@
 
 public class App {
 
-    public static int WIDTH = 400;
+    public static int WIDTH = 500;
+    public static int HEIGHT = 520;
 }
```

The first line is a header. The numbers after the minus sign tells us about which lines has been affected in the src file. The first number represent the start line of the patch. The second number tells us how many lines are affected by the patch. The numbers after the plus sign give us the same information but for the target file.  

The Diff Service turns this string into a JSON array. If several consecutive rows have the same start symbol, they are bundled together into a code part. The JSON array for the example above would be:

```[
   {
      "srcLinesLength":5,
      "startTagretLine":2,
      "codeParts":[
         {
            "nbrOfLines":3,
            "content":"\npublic class App {\n\n",
            "startsWith":" "
         },
         {
            "nbrOfLines":1,
            "coordinates":[
               {
                  "endColumn":34,
                  "startColumn":30
               }
            ],
            "content":"    public static int WIDTH = 400;\n",
            "startsWith":"-"
         },
         {
            "nbrOfLines":2,
            "coordinates":[
               {
                  "endColumn":34,
                  "startColumn":30
               }
            ],
            "content":"    public static int WIDTH = 500;\n    public static int HEIGHT = 520;\n",
            "startsWith":"+"
         },
         {
            "nbrOfLines":1,
            "content":"}\n",
            "startsWith":" "
         }
      ],
      "targetLinesLength":6,
      "startSrcLine":2
   }
]
```

Sometimes, only a small change is done to a row. For instance when WIDTH is changed from 400 to 500 in the example above. The Diff Service calculates how many chars there are between the start of the patch and the start/end of one of these small changes. These values are represented by `startColumn` and `endColumn`.  These values are calculated with the help of the library java-diff-utils. 

**Meaning of different values:**

- srcLinesLength: how many lines this patch will cover in the src file.
- targetLinesLength: how many lines this patch will cover in the target file.
- startSrcLine: at which line in the src file the patch starts at.
- startTargetLine: at which line in the target file the patch starts at.
- codeParts: a list of bundles of code. Each bundle is either in the src file, target file or in both.
- startsWith: the start symbol om the current codePart. 
    - "+": code that is added to target file.
    - "-": code that is removed from the src file.
    - " ": code that occur in both files.
- content: the content in the current codePart.
- nbrOfLines: how many lines the current codePart consists of.
- startColumn: how many chars from the begining of the patch before a small change should begin.
- endColumn: how many chars from the begining of the patch before a small change should end.

